# syntax=docker/dockerfile:experimental
FROM mcr.microsoft.com/devcontainers/python:3.8

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /app
WORKDIR /app

COPY . ./bitfuncs

RUN --mount=type=cache,target=/tmp/.cache/pip \
    apt update \
 && apt install --no-install-recommends --yes \
      curl \
      libpq-dev \
      python3-dev \
      python3-pip \
      python3-numpy \
 && curl -L http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz | tar xz \
 && cd ./ta-lib \
 && ./configure --prefix=/usr \
 && make \
 && make install \
 && cd ../bitfuncs \
 && pip install \
      --cache-dir=/tmp/.cache/pip \
      '.[backtest,bittrex,degiro,yahoo]' \
 && cd .. \
 && rm -rf ../bitfuncs ../ta-lib \
 && apt purge --yes python3-dev \
 && apt autoremove --yes \
 && apt clean \
 && rm -rf /var/lib/apt/lists/*

COPY --chown=nobody:nogroup bitfuncs/sql bitfuncs/sql

USER nobody

CMD ["chamallow", "config.yml"]
