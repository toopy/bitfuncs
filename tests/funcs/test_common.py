from bitfuncs.funcs.common import merge_values_w_ticker


def test_insert():
    values = [{"c": 1, "time": "2024-09-22T00:00:00"}]

    # no values
    assert not merge_values_w_ticker(None, None)

    # no ticker
    assert merge_values_w_ticker(values, None) == values

    # not recent enough
    assert (
        merge_values_w_ticker(
            values,
            {
                "c": 2,
                "time": "2024-09-22T00:00:00",
            },
        )[
            0
        ]["c"]
        == 1
    )

    # merged
    assert (
        merge_values_w_ticker(
            values,
            {
                "c": 2,
                "time": "2024-09-23T00:00:00",
            },
        )[
            0
        ]["c"]
        == 2
    )
