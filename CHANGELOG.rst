CHANGELOG
=========


0.2.1 (unreleased)
------------------

- Nothing changed yet.


0.2.0 (2024-09-22)
------------------

- Add yahoo connector


0.1.0 (2021-07-14)
------------------

- First implementation
